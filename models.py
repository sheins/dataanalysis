## models.py
## here we introduce the model classes that are used for the analysis

from sklearn.feature_selection import RFECV
from sklearn.svm import SVC
from sklearn.neural_network import MLPClassifier
from sklearn.neighbors import KNeighborsClassifier 
from sklearn.gaussian_process import GaussianProcessClassifier
from sklearn.gaussian_process.kernels import RBF
from sklearn.tree import DecisionTreeClassifier 
from sklearn.ensemble import RandomForestClassifier, AdaBoostClassifier
from sklearn.naive_bayes import GaussianNB
from sklearn.discriminant_analysis import QuadraticDiscriminantAnalysis
from sklearn.neural_network import MLPClassifier


## basic Model class, which inherits several methods to the child classes
class Model:
    def __init__(self, model, name=""):
        self.model = model
        self.name = name

    ## basic fit method: take training features and classes
    ## conduct feature pruning if needed
    def fit(self, X_train, y_train, do_feature_pruning = False, prune_steps=0.2, prune_cv = 5 ):
        if do_feature_pruning:
            ## Recursive feature elimination with cross-validation (sklearn)
            selector = RFECV(self.model, step=prune_steps, cv=prune_cv)
            selector = selector.fit(X_train, y_train)
            self.support_indice = selector.support_
            X_train_reduced = X_train[:,self.support_indice]
            ## now the model is based on the reduced features
            self.model.fit(X_train_reduced, y_train)  
        else:
            self.support_indice = [ i for i in range(X_train.shape[1])]
            self.model.fit(X_train, y_train)

    ## predict based on some features coming from X_test
    def predict(self, X_test):
        return self.model.predict(X_test)

    ## get feature importance
    def get_feature_importances(self):
        return self.model.feature_importances_

    ## get the indice that point to the support features
    def get_support_indice(self):
        return self.support_indice

    ## get the model
    def get_model(self):
        return self.model

    ## get the name for the Model
    def get_name(self):
        return self.name


## Model child Random Forest
class RandomForest(Model):
    def __init__(self, n_estimators=10, random_state=0, max_features = "sqrt"):
        model = RandomForestClassifier(n_estimators=n_estimators, random_state=random_state, max_features = max_features)
        super().__init__(model=model, name ="_".join(["RandomForest", str(n_estimators)]) )

## Model child Support Vector Machine
class SVM(Model):
    ## kernels: 'linear', 'rbf' (radial basis function)
    def __init__(self, kernel='linear'):
        self.kernel = kernel
        model = SVC(kernel=kernel)
        super().__init__(model=model, name ="_".join(["SVM", str(kernel)]))

    ## for rfb try to run the fit without feature_pruning first -> did not work yet -> TODO
    def fit(self, X_train, y_train, do_feature_pruning = False, prune_steps=0.2, prune_cv = 5 ):
        if self.kernel == 'rfb':
            self.model.fit(X_train, y_train)
        super().fit(X_train, y_train, do_feature_pruning, prune_steps, prune_cv)

    ## only return feature importances, if it used the linear SVM
    def get_feature_importances(self):
        if self.kernel != 'linear':
            #raise Exception("model kernel is not linear -> its coefficients are not directly related to the input space")
            return None
        return self.model.coef_[0]

## Model child Decision Tree
class DecisionTree(Model):
    def __init__(self, max_depth=5):
        model = DecisionTreeClassifier(max_depth=max_depth)
        super().__init__(model=model, name ="_".join(["DecisionTree", str(max_depth)]))

## Model child KNeighbors
class KNeighbors(Model):
    def __init__(self, n_neighbors=5, weights='uniform', algorithm='auto', leaf_size=30, p=2, metric='minkowski', metric_params=None, n_jobs=None):
        model = KNeighborsClassifier(n_neighbors=n_neighbors, weights= weights, algorithm=algorithm, leaf_size=leaf_size, p=p, metric=metric,metric_params=metric_params,n_jobs=n_jobs)
        super().__init__(model=model, name ="KNeighbors")
    
    def get_feature_importances(self):
        #raise Exception("KNeighbors doesn't provide feature importances or coef")
        return None

## Model child GaussianProcess
class GaussianProcess(Model):
    def __init__(self, kernel=1.0 * RBF(1.0), optimizer='fmin_l_bfgs_b', n_restarts_optimizer=0, max_iter_predict=100, warm_start=False, copy_X_train=True, random_state=None, multi_class='one_vs_rest', n_jobs=None):
        model = GaussianProcessClassifier(kernel=kernel, optimizer=optimizer, n_restarts_optimizer=n_restarts_optimizer, max_iter_predict=max_iter_predict, warm_start=warm_start, copy_X_train=copy_X_train,random_state=random_state,multi_class=multi_class, n_jobs=n_jobs)
        super().__init__(model=model, name ="GaussianProcess")
    
    def get_feature_importances(self):
        #raise Exception("GaussianProcess doesn't provide feature importances or coef")
        return None
    
## Model child Multilayer Perceptron
class MLP(Model):
    def __init__(self, hidden_layer_sizes=100, activation='relu', solver='adam', alpha=0.0001, batch_size='auto', learning_rate='constant', learning_rate_init=0.001, power_t=0.5, max_iter=200, shuffle=True, random_state=None, tol=0.0001, verbose=False, warm_start=False, momentum=0.9, nesterovs_momentum=True, early_stopping=False, validation_fraction=0.1, beta_1=0.9, beta_2=0.999, epsilon=1e-08, n_iter_no_change=10, max_fun=15000):
        model = MLPClassifier(hidden_layer_sizes=hidden_layer_sizes, activation=activation, solver=solver, alpha = alpha, batch_size=batch_size, learning_rate=learning_rate, learning_rate_init=learning_rate_init, power_t=power_t, max_iter=max_iter, shuffle=shuffle, random_state=random_state, tol=tol, verbose=verbose, warm_start=warm_start, momentum=momentum, nesterovs_momentum=nesterovs_momentum, early_stopping=early_stopping, validation_fraction=validation_fraction, beta_1=beta_1, beta_2=beta_2, epsilon=epsilon, n_iter_no_change=n_iter_no_change, max_fun=max_fun)
        super().__init__(model=model, name ="MLP")

    def get_feature_importances(self):
        #raise Exception("GaussianProcess doesn't provide feature importances or coef")
        return None 

## Model child AdaBoost
class AdaBoost(Model):
    def __init__(self, base_estimator=None, n_estimators=50, learning_rate=1.0, algorithm='SAMME.R', random_state=None):
        model = AdaBoostClassifier(base_estimator=base_estimator, n_estimators=n_estimators, learning_rate=learning_rate, algorithm=algorithm, random_state=random_state)
        super().__init__(model, name ="AdaBoost")

## Model child GaussNB
class GaussNB(Model):
    def __init__(self, priors=None, var_smoothing=1e-09):
        model = GaussianNB(priors=priors, var_smoothing=var_smoothing)
        super().__init__(model, name ="GaussNB")

## Model child QuadraticDiscriminantAnalysis
class QDA(Model):
    def __init__(self, priors=None, reg_param=0.0, store_covariance=False, tol=0.0001):
        model=  QuadraticDiscriminantAnalysis(priors=priors, reg_param=reg_param, store_covariance=store_covariance, tol=tol)
        super().__init__(model, name ="QDA")

    def get_feature_importances(self):
        #raise Exception("QuadraticDiscriminantAnalysis doesn't provide feature importances or coef")
        return None 