## helperbiomedical.py
## some biomedical helper functions

import os
import mygene

## use mygene to find metadata about one or more ensembl gene, that results from a feature-importrance-list
#  and write a summary about the returned meta-data and feature-importance score
def write_gene_table(experiment_object):
        ## get relevant feature importance
        final_feature_importances = experiment_object.feature_importance_table.loc[:,experiment_object.feature_importance_table.mean(axis=0) > 0]
        ## sorted list ordered by feature importance (descending)
        ens = final_feature_importances.mean(axis=0).sort_values(axis=0, ascending=False)
        mg = mygene.MyGeneInfo()
        ens_simple = []
        ginfo_done =[]
        ## remove the version of the gene
        for gene in ens.index:
            simple_gene_name = gene.split('.')[0]
            ens_simple.append(simple_gene_name)

        ## start the actual query
        ginfo = mg.querymany(ens_simple, scopes='ensembl.gene')
        i = 0
        ## define and write header
        csv_header = "query;_id;_score;entrezgene;name;symbol;taxid;top_feature_importances;top_feature_importances_index"
        gene_list_file_name = os.path.join(experiment_object.get_path(), "genelist.csv")
        with open(gene_list_file_name, "w") as outfile:
            outfile.write(csv_header+"\n")
            outfile.close()

        ## for each query entry, get the information
        for index, g in enumerate(ginfo):
            ## remove duplicates: sometimes there are two or more results returned from one query
            if not g['query'] in ginfo_done:
                line = ""
                for header in csv_header.split(";")[0:-2]:
                    if header in g.keys():
                        line = line + "{0};".format(g[header])
                    else:
                        line = line + "{0};".format("NA")
                line = line + str(ens[i]) +";" +str(i)
                ## write result to genelist.csv
                with open(gene_list_file_name, 'a') as outfile:
                    outfile.write(line+"\n")
                    outfile.close()
                ginfo_done.append(g['query'])
                i += 1 