## helper.py
## some useful helper functions

from __future__ import print_function

## general imports
import logging
import sys
import numpy as np
import os
import copy

## plot
#%matplotlib inline
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import seaborn as sns


## sklearn
from sklearn.metrics import confusion_matrix
from sklearn.utils.multiclass import unique_labels

## numpy
import numpy as np 


np.set_printoptions(precision=2)


## define a logger and return it for further output
def setup_custom_logger(name, path="./"):
    formatter = logging.Formatter(fmt='%(asctime)s %(levelname)-8s %(message)s',
                                  datefmt='%Y-%m-%d %H:%M:%S')
    handler = logging.FileHandler(path+'log.txt', mode='w')
    handler.setFormatter(formatter)
    screen_handler = logging.StreamHandler(stream=sys.stdout)
    screen_handler.setFormatter(formatter)
    
    logger = logging.getLogger(name)
    logger.setLevel(logging.DEBUG)
    logger.handlers = []
    logger.addHandler(handler)
    logger.addHandler(screen_handler)
    return logger

## plot the default confusion matrix
def plot_confusion_matrix(y_true, y_pred, classes,
                          normalize=False,
                          title=None,
                          cmap=plt.cm.Blues, caption='', path='./'):
    """
    This function prints and plots the confusion matrix.
    Normalization can be applied by setting `normalize=True`.
    """
    if not title:
        if normalize:
            title = 'Normalized confusion matrix '+caption
        else:
            title = 'Confusion matrix, without normalization '+caption
    # Compute confusion matrix
    cm = confusion_matrix(y_true, y_pred)
    # Only use the labels that appear in the data
    classes = classes[unique_labels(y_true, y_pred)]
    if normalize:
        cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]
        print("Normalized confusion matrix")
        postfix = 'normalized'
    else:
        print('Confusion matrix, without normalization')
        postfix = 'unnormalized'
    
    ## start the figure
    fig = plt.figure(figsize=(25, 25))
    fig.set_size_inches(18.5, 10.5, forward=True)
    ax = fig.add_subplot(111, aspect='equal')
    im = ax.imshow(cm, interpolation='nearest', cmap=cmap)
    ax.figure.colorbar(im, ax=ax)
    # We want to show all ticks...
    ax.set(xticks=np.arange(cm.shape[1]),
           yticks=np.arange(cm.shape[0]),
           # ... and label them with the respective list entries
           xticklabels=classes, yticklabels=classes,
           title=title,
           ylabel='True label',
           xlabel='Predicted label')
    # Rotate the tick labels and set their alignment.
    plt.setp(ax.get_xticklabels(), rotation=45, ha="right",
             rotation_mode="anchor")
    ax.autoscale()
    figure_name_caption = ''
    if len(caption) > 0:
        figure_name_caption = caption
    plt.savefig(os.path.join(path, 'confusion_matrix_'+figure_name_caption+'_'+postfix+'.png'), dpi = 200)
    return ax

## some user defined color_codes, which could help improve clarity for different plots
def color_codes():
    return [            #"#9b59b6", "#2871a0", 
            "#e74c3c","#2a00ff", "#000000", "#2ecc71", "#009cff", "#f51292", "#f5e312", "#fd8282", "#00fffc", "#f000ff",  
            "#12f570","#f5b812","#b812f5", "#f59cce", "#28a028","#b5b42d","#7c213e","#7cf53e","#328263","#666666", "#b8b8b8", 
            "#fd8000","#a67500","#ffce58", "#ff0000", "#c30000", "#009615","#0086e3",
    ]


## replace NaN entries in a dataframe by the median or mean
def replace_nan_in_dataframe(df, axis=1, method = 'median'):
    df_return = None
    ## only median and mean are supported
    if method in ['median', 'mean']:
        ## copy the original dataframe    
        df_return = copy.copy(df)
        is_NaN = df_return.isnull()
        ## identify rows or cols, that have Nan values
        row_or_col_has_NaN = is_NaN.any(axis=axis)
        if axis == 1:
            ## for each row:
            for i in range(df_return.shape[0]):
                ## determine median / mean for each row
                median_nan_row = df_return.median(axis=axis).iloc[i] if method == 'median' else df_return.mean(axis=axis).iloc[i]
                df_corrected_row = df_return.iloc[i,:].fillna(median_nan_row) 
                ## replace the row by corrected row
                df_return.iloc[i,:] = df_corrected_row
        elif axis == 0:
            ## for each col:
            for i in range(df_return.shape[1]):
                ## determine median / mean for each col
                median_nan_col = df_return.median(axis=axis).iloc[i] if method == 'median' else df_return.mean(axis=axis).iloc[i]
                df_corrected_col = df_return.iloc[:,i].fillna(median_nan_col) 
                ## replace the col by corrected col
                df_return.iloc[:,i] = df_corrected_col
        else:
            ## wrong axis
            raise Exception("wrong value for axis, should be in [0,1], got: "+str(axis))
    else:
        ## wrong method
        raise Exception("wrong method: must be in ['median', 'mean']")
    return df_return