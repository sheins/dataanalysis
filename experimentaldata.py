## experimentaldata.py
## defines the class ExperimentalData, which is used for the Experiment
## it must provide a suitable format in order to run the experiment for any model

from __future__ import print_function

## general imports
import pandas as pd


## scikit learn
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import classification_report, confusion_matrix, accuracy_score

from sklearn import svm, datasets
from sklearn.model_selection import train_test_split
from sklearn.metrics import confusion_matrix
from sklearn.utils.multiclass import unique_labels

#from sklearn.datasets import fetch_mldata
from sklearn.decomposition import PCA
from sklearn.manifold import TSNE

## plot
#%matplotlib inline
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import seaborn as sns


## keras
import keras
from keras.datasets import mnist
from keras.models import Sequential
from keras.layers import Dense, Dropout
from keras.optimizers import RMSprop


## contains all important data of ExperimentalData (features, annotations)
class ExperimentalData:
    def __init__(self, logger, count_file_name="", annotation_file_name="", index_string="", sep_data=',',sep_annotation=',', skiprows_data=0, skiprows_annotation=0):
        self.logger = logger
        self.logger.info('Initializing Expression instance')

        if (len(count_file_name) > 0):
            ## data matrix
            self.data = pd.read_csv(count_file_name, skiprows=skiprows_data, sep=sep_data)
            ## transcript count matrix

        self.logger.info('Read the annotation file')
        self.label_table = pd.read_csv(annotation_file_name, skiprows=skiprows_annotation, sep=sep_annotation)
        self.logger.info('DONE reading the csv files')
        
        temp_datamatrix = self.data.iloc[:,:]
        temp_datamatrix = temp_datamatrix.set_index(index_string)

        self.logger.info('START transposing matrix')
        self.datamatrix = temp_datamatrix.transpose()
        
        self.logger.info('DONE transposing matrix')
        self.logger.info('DONE Initializing Gtex instance')
        
    ## create the datamatrix from annotions and certain attributes, group definitions
    def create_datamatrix(self, attribute = 'SMTSD', sample_id_attribute = 'SAMPID', group_definitions = None, apply_scaling = False):        
        self.attribute = attribute
        self.logger.info('Start inserting annotations into transpose_gene_matrix')

        ## insert the annotations
        annot_class_int = pd.Series([]) 
        for i in range(self.datamatrix.shape[0]): 
            if (group_definitions != None):    
                annotation_str = list(self.label_table.loc[self.label_table[sample_id_attribute] == self.datamatrix.index[i], attribute])[0]
                if (annotation_str in group_definitions):
                    annot_class_int[i] = group_definitions[annotation_str]
                else:
                    annot_class_int[i] = "remove_group"
            else:
                annot_class_int[i] = list(self.label_table.loc[self.label_table[sample_id_attribute] == self.datamatrix.index[i], attribute])[0]
        
        annot_class_int.index = self.datamatrix.index
        
        ## create the annotated datamatrix, which is exactly the datamatrix with an addtional column "label", which contains the int-version of classes
        self.annot_datamatrix = self.datamatrix.copy()
        self.annot_datamatrix.insert(self.annot_datamatrix.shape[1], "label", annot_class_int)

        ## define label mapping
        self.label_mapping = self.annot_datamatrix['label'].unique()
        self.dict_mapping = dict(zip(self.label_mapping, range(len(self.label_mapping))) )
        self.dict_mapping_inv = {v: k for k, v in self.dict_mapping.items()}
        
        self.logger.info('Starting changing the labels matrix to int values')
        self.annot_datamatrix['label'] = self.annot_datamatrix.apply(lambda df: int(df.label.replace(df.label, str(self.dict_mapping[df.label]))), axis=1)

        self.logger.info('DONE changing the labels matrix to int values')
        self.logger.info('DONE annotation')

        ## exclude "remove_group" samples from the datamatrix
        if ("remove_group" in self.dict_mapping):
            ## remove samples that match the exclude classes list
            self.datamatrix = self.datamatrix.loc[ [self.dict_mapping_inv[i] != 'remove_group' for i in self.annot_datamatrix['label']], :]
            self.annot_datamatrix = self.annot_datamatrix.loc[ [self.dict_mapping_inv[i] != 'remove_group' for i in self.annot_datamatrix['label']], :]

        ## create the scaled datamatrix
        pre_scale_datamatrix = self.datamatrix
        if (apply_scaling):
            self.logger.info('Start scaling to (0,1)')
            self.datamatrix = pre_scale_datamatrix.apply(lambda x:(x.astype(float) - min(x))/(max([max(x)-min(x),1])), axis = 1)
            self.logger.info('DONE scaling to (0,1)')
        else:
            self.logger.info('No scaling was applied to the input data')
        
        ## create the annotated sclaed datamatrix
        annot_labels = self.annot_datamatrix['label']
        self.annot_datamatrix = self.datamatrix.copy()
        self.annot_datamatrix.insert(self.annot_datamatrix.shape[1], "label", annot_labels)
         

    ## some getters

    def get_data(self):
        return self.datamatrix
    
    def get_annotated_data(self):
        return self.annot_datamatrix

    def get_annotations(self):
        return self.annot_datamatrix['label']

    def get_features(self):
        return self.get_data().columns
    
    def get_dict_mapping(self):
        return self.dict_mapping
    
    def get_dict_mapping_inv(self):
        return self.dict_mapping_inv
    
    def get_label_mapping(self):
        return self.label_mapping
    
    def get_attribute(self):
        return self.attribute
    