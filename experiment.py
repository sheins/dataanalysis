## experiment.py
## The experiement uses ExperimentalData and a model in order to conduct an analasis 
## and provide insight by plotting different figures

from __future__ import print_function

## general imports
import logging
import sys
import numpy as np
import pandas as pd
import time
import copy
import os

## scikit learn
from sklearn.preprocessing import StandardScaler
from sklearn.metrics import precision_recall_fscore_support
from sklearn.decomposition import PCA
from sklearn.manifold import TSNE
from sklearn.model_selection import StratifiedKFold
from sklearn.metrics import plot_roc_curve
from sklearn.metrics import auc

## scipy
from scipy import interp

## plot
#%matplotlib inline
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import seaborn as sns

from dataanalysis.helper import setup_custom_logger
from dataanalysis.helper import plot_confusion_matrix
from dataanalysis.helper import color_codes

from imblearn.over_sampling import SMOTE
from sklearn.preprocessing import StandardScaler
from sklearn.model_selection import StratifiedKFold
from dataanalysis.experimentaldata import ExperimentalData

from dataanalysis.models import Model

## an Experiment is created by taking experimental data and adding a model to it
## kfold is the number used for kfold cross validation
## shuffle is used for creating the different kfolds
class Experiment:
    def __init__(self, experimental_data, model, name= "experiment", project_path="./reports/", logger_name = "logger", kfold = 3, random_state = 0, shuffle = False):
        #assert type(experimental_data) == ExperimentalData, "wrong data type for experimental_data: " + str(type(experimental_data)) + " instead of dataanalysis.experimentaldata.ExperimentalData"
        
        self.name = name
        self.path = os.path.join(project_path, name, model.get_name())
        self.experimental_data = experimental_data
        self.X = experimental_data.get_data()
        self.y = experimental_data.get_annotations()
        self.features = experimental_data.get_features()
        self.kfold = kfold
        self.results = []
        self.model = model
        ## create the report path if it doesn't exist
        if not os.path.exists(self.path):
            os.makedirs(self.path)
        self.logger = setup_custom_logger(logger_name, self.path+"/")

        ## kfold must be >= 1
        if kfold < 1:
            raise Exception("kfold must be >=  ")
        ## if there is only one fold, we use the complete data as train and test
        elif kfold == 1:
            train_index = [i for i in range(experimental_data.get_data().shape[0])]
            test_index = train_index
            self.kfolds = [[train_index, test_index]]
        ## if there is more than one fold, we use the kfolds as train and test
        else:
            kf = StratifiedKFold(n_splits=kfold, random_state = random_state, shuffle=shuffle)
            self.kfolds = []
            for (train_index, test_index) in kf.split(self.X, self.y):
                self.kfolds.append([train_index, test_index])
            
    ## run a certain kfold_index (train set + test set are used)
    ## feature pruning: reduce the features to a minimum degree
    ## oversampling is also supported (do_oversampling_smaller_group = True 
    ## -> SMOTE is used with smote_neighbors being the amount of neighbors that are used for the creation
    ## of interpolated artificial samples
    def run_kfold(self, kfold_index, random_state, do_feature_pruning = False, prune_steps=0.2, prune_cv = 5,  do_standard_scaling = False, do_oversampling_smaller_group = False, smote_neighbors = 5):
        type(self.model) == Model, "wrong data type for model: " + str(type(Model)) + " instead of dataanalysis.models.Model"
        #self.logger = setup_custom_logger(self.logger_name, os.path.join)
        
        X_train, X_test = self.get_kfold_X(kfold_index)
        y_train, y_test = self.get_kfold_y(kfold_index)
        
        ## scales the measurements to the interval [0,1]
        if do_standard_scaling:
            # Feature Scaling
            sc = StandardScaler()  
            X_train = sc.fit_transform(X_train)  
            X_test = sc.transform(X_test)  
        
        ## oversample the smaller group(s)
        if do_oversampling_smaller_group:
                ## oversampling on smaller group
                smote = SMOTE(sampling_strategy='auto', k_neighbors=smote_neighbors, random_state=random_state)
                X_train, y_train = smote.fit_resample(X_train, y_train)

        ## copy the original model because we want to explicitely return the model, that has been trained
        ## if we don't and we on top feature prune, we'll get an error since some features are dismissed
        model_processed = copy.deepcopy(self.model)
        model_processed.fit(X_train, y_train, do_feature_pruning = do_feature_pruning, prune_steps=prune_steps, prune_cv = prune_cv )    
        y_pred = model_processed.predict(X_test[:,model_processed.get_support_indice()])

        ## get feature importance from the processed model
        feature_importances = model_processed.get_feature_importances()
        feature_names = self.features[model_processed.get_support_indice()]

        ## create the precision_recall_fscore_support statistics for the test vs predicted results
        statistics = precision_recall_fscore_support(y_test,y_pred)
        self.logger.info("[precision recall fscore support] = " + str(statistics))
        self.logger.info("feature_names: " + str(feature_names))
        self.logger.info("feature_importances: " + str(feature_importances))
        self.logger.info("DONE running the model")

        ## return the model ('regressor'), the test features ('X_test'), the test classes ('y_test'), the real classes ('y_pred')
        ## , feature importances, corresponding feature_names and statistics
        dict_return = {'regressor': model_processed, 'X_test': X_test, 'y_test': y_test, 'y_pred': y_pred, 'feature_importances': feature_importances, 'feature_names': feature_names, 'precision_recall_fscore_support':statistics}
        return dict_return
    
    ## run all kfolds for one experiment with some extra plots (tsne, pca, confusion_matrices) and parameters, that are needed for the run_kfold function
    def run_model(self, do_plot_tsne_PCA = True, do_plot_ROC= True, do_plot_feature_importances= True, do_plot_confusion_matrix = True, random_state=0, do_feature_pruning = True, prune_steps=0.2, prune_cv = 5, do_standard_scaling= True, do_oversampling_smaller_group= True, smote_neighbors = 3):
        self.results = []
        all_features = list(self.experimental_data.get_features())
        self.feature_importance_table = pd.DataFrame(0, index=np.arange(self.kfold), columns=all_features)

        ## for each kfold do:
        for kfold_index in range(self.kfold):
            self.results.append(self.run_kfold( random_state = random_state, kfold_index=kfold_index, do_feature_pruning = do_feature_pruning, prune_steps=prune_steps, prune_cv = prune_cv, do_standard_scaling = do_standard_scaling, do_oversampling_smaller_group = do_oversampling_smaller_group, smote_neighbors = smote_neighbors))
            y_pred = self.results[kfold_index]['y_pred']
            y_train, y_test = self.get_kfold_y(index=kfold_index)
            
            ## plot confusion matrix
            if do_plot_confusion_matrix:
                plot_confusion_matrix(y_true=y_test, y_pred=y_pred, classes=self.experimental_data.label_mapping, normalize= True, title = "confusion_matrix", caption="kfold_"+str(kfold_index), path= self.get_path())

            ## plot tsne and PCA
            if do_plot_tsne_PCA:
                self.plot_tsne(name_prefix = "kfold_"+str(kfold_index)+"_", support_features = True, support_kfold = kfold_index)
                self.plot_PCA(name_prefix = "kfold_"+str(kfold_index)+"_", support_features = True, support_kfold = kfold_index)
            kfold_features = self.results[kfold_index]['feature_names']
            kfold_feature_importances = self.results[kfold_index]['feature_importances']

            ## create the feature importance data for existing features for one kfold
            for index, feature in enumerate(kfold_features):
                self.feature_importance_table.loc[kfold_index,feature] = kfold_feature_importances[index] if kfold_feature_importances[index] != None else 0
        
        ## plot the aggregated feature importance figure (boxplot)
        if do_plot_feature_importances:
            self.plot_feature_importances()
        ## plot ROC-curve
        if do_plot_ROC:
            self.plot_ROC(self.results)


    ## plot confusion matrix for a certain set of y_true, y_pred
    def plot_confusion_matrix(self, y_true, y_pred, normalize= True, title = "confusion matrix", caption="caption"):
        classes = self.experimental_data.label_mapping
        plot_confusion_matrix(y_true=y_true, y_pred=y_pred, classes=classes, normalize= normalize, title = title, caption=caption, path= self.path)
    
    ## plot feature importances (box plot)
    def plot_feature_importances(self):
        ## feature importance plot
        dpi = 200
        final_feature_importances_long = self.feature_importance_table.loc[:,self.feature_importance_table.mean(axis=0) > 0]
        final_feature_importances_long = final_feature_importances_long.reindex(final_feature_importances_long.mean(axis=0).sort_values(ascending=False).index, axis=1)
        fig = plt.figure(figsize=(min([(2**16)/200 - 1, 6+(len(list(final_feature_importances_long.columns))/2)]), 6))
        final_feature_importances_long.boxplot(column = list(final_feature_importances_long.columns))
        plt.xticks(rotation=90)
        plt.savefig(os.path.join(self.get_path(), 'feature_importances.png'), dpi = dpi)

    ## plot ROC-curve, which takes the regressor, X_test, y_test 
    def plot_ROC(self, results):
        ## roc curve variables
        tprs = []
        aucs = []
        mean_fpr = np.linspace(0, 1, 100)
        fig_roc, ax_roc = plt.subplots()

        ## for each kfold, do:
        for kfold_index in range(self.get_kfold()):
        ## ROC curve visualization
            regressor = results[kfold_index]['regressor'].get_model()
            support_indice = results[kfold_index]['regressor'].get_support_indice()
            X_test = results[kfold_index]["X_test"][:, support_indice]
            y_test = results[kfold_index]['y_test']
            viz = plot_roc_curve(regressor, X_test, y_test,
                            name='ROC fold {}'.format(kfold_index),
                            alpha=0.3, lw=1, ax=ax_roc)
            interp_tpr = interp(mean_fpr, viz.fpr, viz.tpr)
            interp_tpr[0] = 0.0
            tprs.append(interp_tpr)
            aucs.append(viz.roc_auc)


        ## ROC Plot
        ax_roc.plot([0, 1], [0, 1], linestyle='--', lw=2, color='r',
        label='Chance', alpha=.8)

        mean_tpr = np.mean(tprs, axis=0)
        mean_tpr[-1] = 1.0
        mean_auc = auc(mean_fpr, mean_tpr)
        std_auc = np.std(aucs)
        ax_roc.plot(mean_fpr, mean_tpr, color='b',
                label=r'Mean ROC (AUC = %0.2f $\pm$ %0.2f)' % (mean_auc, std_auc),
                lw=2, alpha=.8)

        std_tpr = np.std(tprs, axis=0)
        tprs_upper = np.minimum(mean_tpr + std_tpr, 1)
        tprs_lower = np.maximum(mean_tpr - std_tpr, 0)
        ax_roc.fill_between(mean_fpr, tprs_lower, tprs_upper, color='grey', alpha=.2,
                        label=r'$\pm$ 1 std. dev.')

        ax_roc.set(xlim=[-0.05, 1.05], ylim=[-0.05, 1.05],
                title="Receiver operating characteristic")
        ax_roc.legend(loc="lower right")
        plt.savefig(os.path.join(self.get_path(), 'ROC.png'), dpi = 200)

    ## plot the tsne figure
    def plot_tsne(self, name_prefix, support_features = False, support_kfold = None):  
        ## plotting the support features means reducing the input to the support-features only
        if support_features:
            selection = list(self.get_results()[support_kfold]['regressor'].get_support_indice())
            X = self.experimental_data.get_data().loc[:,selection]
        else:
            X = self.experimental_data.get_data()

        ## y is the real class names
        y = [ self.experimental_data.dict_mapping_inv[i] for i in self.experimental_data.get_annotations()]
        ## count the unique classes
        count_classes = len(list(set(list(y))))
        
        ## define the TSNE method
        tsne = TSNE(n_components=2, verbose=1, random_state=123, perplexity=5, n_iter=1000)
        ## just take the top50 PCA components in order to calculate the tsne
        pca_50 = PCA(n_components=min(min(X.shape[0], X.shape[1]), 50))
        pca_result_50 = pca_50.fit_transform(X)
        self.logger.info('Cumulative explained variation for 50 principal components: {}'.format(np.sum(pca_50.explained_variance_ratio_)))
        tsne_pca_results = tsne.fit_transform(pca_result_50)

        ## create the tsne_dataframe with its results
        tsne_df = pd.DataFrame()
        tsne_df["y"] = y
        tsne_df["comp-1"] = tsne_pca_results[:,0]
        tsne_df["comp-2"] = tsne_pca_results[:,1]
        
        ## seaborn scatterplot
        plt.figure(figsize=(10,10))
        sns.scatterplot(
            x="comp-1", y="comp-2", hue=tsne_df.y.tolist(),
            palette=sns.color_palette(color_codes(), count_classes),
            data = tsne_df,
            #alpha=0.3 ## just if there are too many dots to get a better idea of overlapping dots
        )
        ## put the legend in the upper left corner
        plt.legend(loc='upper left')
        plt.savefig(self.path+"/"+name_prefix+'t-SNE.png', dpi = 200)
        
    
    ## plot a PCA
    def plot_PCA(self, name_prefix, support_features = False, support_kfold = None):
        ## plotting the support features means reducing the input to the support-features only
        if support_features:
            selection = list(self.get_results()[support_kfold]['regressor'].get_support_indice())
            X = self.experimental_data.get_data().loc[:,selection]
        else:
            X = self.experimental_data.get_data()
        y = [ self.experimental_data.dict_mapping_inv[i] for i in self.experimental_data.get_annotations()]
        count_classes = len(list(set(list(y))))
        n_components = min(3,min(X.shape[0], X.shape[1]))
        
        ## define the PCA method
        pca = PCA(n_components=n_components)
        pca_result = pca.fit_transform(X)
        
        ## create the PCA with its results
        self.pca_df = pd.DataFrame()
        self.pca_df['pca-one'] = pca_result[:,0]
        self.pca_df['pca-two'] = pca_result[:,1] if n_components > 1 else pca_result[:,0]
        self.pca_df['pca-three'] = pca_result[:,2] if n_components > 2 else pca_result[:,0]

        self.logger.info('Explained variation per principal component: {}'.format(pca.explained_variance_ratio_))
        ## determine classes for the coloring
        self.pca_df['y'] = y    
        
        plt.figure(figsize=(10,10))
        sns.scatterplot(
            x="pca-one", y="pca-two",
            hue="y",
            palette=sns.color_palette(color_codes(), count_classes), ## color_palette 'hls' is also nice
            data = self.pca_df,
            #alpha=0.3
        )
        plt.legend(loc='upper left')
        plt.savefig(self.path+"/"+name_prefix+'PCA.png', dpi = 200)


    ## some getter functions

    def get_X(self):
        return self.X

    def get_y(self):
        return self.y
        
    def get_kfold(self):
        return self.kfold

    def get_kfold_X(self, index):
        return self.get_X().iloc[self.get_kfold_train(index),:], self.get_X().iloc[self.get_kfold_test(index), :]

    def get_kfold_y(self, index):
        return self.get_y()[self.get_kfold_train(index)], self.get_y()[self.get_kfold_test(index)]

    def get_kfold_train(self, index):
        return self.kfolds[index][0]

    def get_kfold_test(self, index):
        return self.kfolds[index][1]
    
    def get_all_kfolds(self):
        return self.kfolds

    def get_name(self):
        return self.name
    
    def get_path(self):
        return self.path

    def get_results(self):
        return self.results
    
    def get_model(self):
        return self.model